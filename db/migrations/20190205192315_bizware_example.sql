-- migrate:up
CREATE TABLE "payments"."bizware_0002" (
  "id" BIGSERIAL PRIMARY KEY,
  "name" varchar(30) COLLATE "pg_catalog"."default"
);

-- migrate:down
DROP TABLE "payments"."bizware_0002"; 
