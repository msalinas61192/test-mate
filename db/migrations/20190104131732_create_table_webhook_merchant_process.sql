-- migrate:up
CREATE TABLE "payments"."webhook_merchant_process" (
  "id" SERIAL PRIMARY KEY,
  "merchant_id" int8,
  "url" varchar(300) COLLATE "pg_catalog"."default",
  "status_id" int4
)
;
COMMENT ON COLUMN "payments"."webhook_merchant_process"."status_id" IS 'Estado de webhook';
COMMENT ON TABLE "payments"."webhook_merchant_process" IS 'Lista de webhook a ejecutar por estado de proceso y merchant.';

-- migrate:down
DROP TABLE IF EXISTS "payments"."webhook_merchant_process";

