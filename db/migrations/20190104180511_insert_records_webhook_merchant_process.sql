-- migrate:up
INSERT INTO "payments"."webhook_merchant_process" VALUES (5, 123, 'http://localhost:8070/v1/webhooks/cards/validation', 1);
INSERT INTO "payments"."webhook_merchant_process" VALUES (7, 15, 'http://sonda-api/v1/webhooks/cards/validation', 1);
INSERT INTO "payments"."webhook_merchant_process" VALUES (9, 15, 'https://api.staging.multicajadigital.cloud/v1/webhooks/cards/ra', 2);

-- migrate:down
DELETE FROM "payments"."webhook_merchant_process" WHERE id = 5;
DELETE FROM "payments"."webhook_merchant_process" WHERE id = 7;
DELETE FROM "payments"."webhook_merchant_process" WHERE id = 9;


