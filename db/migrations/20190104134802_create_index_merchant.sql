-- migrate:up
CREATE UNIQUE INDEX "merchant_id_index" ON "payments"."merchant" USING btree (
  "id" "pg_catalog"."int4_ops" ASC NULLS LAST,
  "id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- migrate:down
DROP INDEX "payments"."merchant_id_index";
