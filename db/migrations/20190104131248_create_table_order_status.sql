-- migrate:up
CREATE TABLE "payments"."order_status" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar(50) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "payments"."order_status"."id" IS 'Id estado de orden';
COMMENT ON COLUMN "payments"."order_status"."name" IS 'Nombre estado de orden.';

-- migrate:down
DROP TABLE IF EXISTS "payments"."order_status";
