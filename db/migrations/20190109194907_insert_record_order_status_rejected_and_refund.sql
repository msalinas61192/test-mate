-- migrate:up
INSERT INTO "payments"."order_status" VALUES (5, 'rejected');
INSERT INTO "payments"."order_status" VALUES (6, 'refund');

-- migrate:down
DELETE FROM "payments"."order_status" where id in(5,6);
