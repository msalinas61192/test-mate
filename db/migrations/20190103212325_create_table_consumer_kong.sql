-- migrate:up
CREATE TABLE "payments"."consumer_kong" (
  "id" BIGSERIAL PRIMARY KEY,
  "merchant_id" int8,
  "kong_id" varchar(50) COLLATE "pg_catalog"."default",
  "algorithm_kong" varchar(50) COLLATE "pg_catalog"."default",
  "key_kong" varchar(50) COLLATE "pg_catalog"."default",
  "secret_kong" varchar(50) COLLATE "pg_catalog"."default",
  "consumer_kong" varchar(50) COLLATE "pg_catalog"."default",
  "api_key" varchar(50) COLLATE "pg_catalog"."default",
  "iss" varchar(50) COLLATE "pg_catalog"."default",
  "apikey" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "payments"."consumer_kong"."id" IS 'Id del kong merchant (consumer)';
COMMENT ON COLUMN "payments"."consumer_kong"."merchant_id" IS 'Id del merchant.';
COMMENT ON COLUMN "payments"."consumer_kong"."kong_id" IS 'Id creacion merchant (consumer) Kong.';
COMMENT ON COLUMN "payments"."consumer_kong"."algorithm_kong" IS 'Id algoritmo codificación merchant (consumer) Kong.';
COMMENT ON COLUMN "payments"."consumer_kong"."key_kong" IS 'Id para generación token Kong.';
COMMENT ON COLUMN "payments"."consumer_kong"."secret_kong" IS 'Llave de encriptación token Kong.';
COMMENT ON COLUMN "payments"."consumer_kong"."consumer_kong" IS 'Id merchant (consumer) en kong.';
COMMENT ON COLUMN "payments"."consumer_kong"."api_key" IS 'Id (permamente) para consumir APIs.';
COMMENT ON COLUMN "payments"."consumer_kong"."iss" IS 'Identificador para consumir API.';


-- migrate:down
DROP TABLE IF EXISTS "payments"."consumer_kong";
