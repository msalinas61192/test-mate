-- migrate:up
INSERT INTO "payments"."merchant_method" VALUES (1, 'MP6541', 15);
INSERT INTO "payments"."merchant_method" VALUES (2, 'MP564', 15);

-- migrate:down
DELETE FROM "payments"."merchant_method" WHERE id = 1;
DELETE FROM "payments"."merchant_method" WHERE id = 2;
