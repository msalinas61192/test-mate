-- migrate:up
ALTER TABLE "payments"."event_order" ADD CONSTRAINT "fk_order_id" FOREIGN KEY ("id_order") REFERENCES "payments"."order" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;


-- migrate:down
ALTER TABLE "payments"."event_order" DROP CONSTRAINT "fk_order_id";
