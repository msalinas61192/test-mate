-- migrate:up
ALTER TABLE "payments"."item" ADD CONSTRAINT "item_order_id_fkey" FOREIGN KEY ("order_id") REFERENCES "payments"."order" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- migrate:down
ALTER TABLE "payments"."item" DROP CONSTRAINT "item_order_id_fkey";

