-- migrate:up
INSERT INTO "payments"."method" VALUES (1, 'MP564', 'tarjetas', NULL, 'medio de pago asociado a comercio Z.', 'f', 150000, 't');
INSERT INTO "payments"."method" VALUES (2, 'MP6541', 'paypal', NULL, 'medio de pago asociado a comercio X.', 'f', 36547, 't');

-- migrate:down
DELETE FROM "payments"."method" WHERE id = 1;
DELETE FROM "payments"."method" WHERE id = 2;
