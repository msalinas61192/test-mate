-- migrate:up
CREATE UNIQUE INDEX "method_id_index" ON "payments"."method" USING btree (
  "id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "method_code_index" ON "payments"."method" USING btree (
  "code"
);

-- migrate:down
DROP INDEX "payments"."method_id_index";
DROP INDEX "payments"."method_code_index";
