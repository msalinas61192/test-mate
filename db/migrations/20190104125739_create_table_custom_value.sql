-- migrate:up
CREATE TABLE "payments"."custom_value" (
  "id" BIGSERIAL PRIMARY KEY,
  "key" varchar(30) COLLATE "pg_catalog"."default",
  "value" varchar(300) COLLATE "pg_catalog"."default",
  "order_id" int8 NOT NULL
)
;
COMMENT ON COLUMN "payments"."custom_value"."id" IS 'Id custom value.';
COMMENT ON COLUMN "payments"."custom_value"."key" IS 'Llave del registro.';
COMMENT ON COLUMN "payments"."custom_value"."value" IS 'Valor del registro.';
COMMENT ON COLUMN "payments"."custom_value"."order_id" IS 'Id de orden.';

-- migrate:down
DROP TABLE IF EXISTS "payments"."custom_value";
