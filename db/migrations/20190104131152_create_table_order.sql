-- migrate:up
CREATE TABLE "payments"."order" (
  "id" BIGSERIAL PRIMARY KEY,
  "reference_id" varchar(30) COLLATE "pg_catalog"."default",
  "description" varchar(300) COLLATE "pg_catalog"."default",
  "redirect_url" varchar(300) COLLATE "pg_catalog"."default",
  "status_id" int4,
  "url_return" varchar(300) COLLATE "pg_catalog"."default",
  "url_cancel" varchar(300) COLLATE "pg_catalog"."default",
  "method_id" int4,
  "method_list" json,
  "merchant_id" int8 NOT NULL,
  "payer" json,
  "total" int8 DEFAULT 0,
  "currency" varchar(3) COLLATE "pg_catalog"."default",
  "total_details" json,
  "creation_date" timestamp(6),
  "modification_date" timestamp(6)
)
;
COMMENT ON COLUMN "payments"."order"."id" IS 'Id orden.';
COMMENT ON COLUMN "payments"."order"."reference_id" IS 'Identificacion del merchant (comercio).';
COMMENT ON COLUMN "payments"."order"."description" IS 'Descripcion de la orden.';
COMMENT ON COLUMN "payments"."order"."redirect_url" IS 'URL redireccion pasarela Multicaja.';
COMMENT ON COLUMN "payments"."order"."status_id" IS 'Estado de orden.';
COMMENT ON COLUMN "payments"."order"."url_return" IS 'URL redireccion merchant.';
COMMENT ON COLUMN "payments"."order"."url_cancel" IS 'URL cancelación merchant.';
COMMENT ON COLUMN "payments"."order"."method_id" IS 'Id tipo de pago seleccionado.';
COMMENT ON COLUMN "payments"."order"."method_list" IS 'Listado tipos de pago merchant.';
COMMENT ON COLUMN "payments"."order"."merchant_id" IS 'Id comercio.';
COMMENT ON COLUMN "payments"."order"."payer" IS 'Identificador del cliente que efectua pago de una orden.';
COMMENT ON COLUMN "payments"."order"."creation_date" IS 'Fecha de creacion orden.';
COMMENT ON COLUMN "payments"."order"."modification_date" IS 'Fecha de modificacion orden.';


-- migrate:down
DROP TABLE IF EXISTS "payments"."order";
