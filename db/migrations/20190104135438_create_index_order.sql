-- migrate:up
CREATE INDEX "order_merchant_id_index" ON "payments"."order" USING btree (
  "merchant_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE INDEX "order_method_id_index" ON "payments"."order" USING btree (
  "method_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "order_reference_id_index" ON "payments"."order" USING btree (
  "reference_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "order_status_id_index" ON "payments"."order" USING btree (
  "status_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- migrate:down
DROP INDEX "payments"."order_merchant_id_index";
DROP INDEX "payments"."order_method_id_index";
DROP INDEX "payments"."order_reference_id_index";
DROP INDEX "payments"."order_status_id_index";
