-- migrate:up
ALTER TABLE "payments"."merchant_method" ADD CONSTRAINT "merchant_method_merchant_code_fkey" FOREIGN KEY ("merchant_code") REFERENCES "payments"."merchant" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "payments"."merchant_method" ADD CONSTRAINT "merchant_method_method_code_fkey" FOREIGN KEY ("method_code") REFERENCES "payments"."method" ("code") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- migrate:down
ALTER TABLE "payments"."merchant_method" 
  DROP CONSTRAINT "merchant_method_merchant_code_fkey",
  DROP CONSTRAINT "merchant_method_method_code_fkey";
