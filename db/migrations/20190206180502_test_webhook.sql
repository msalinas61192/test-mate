-- migrate:up
CREATE TABLE "payments"."test_webhook_001" (
  "id" BIGSERIAL PRIMARY KEY,
  "name" varchar(30) COLLATE "pg_catalog"."default"
);

-- migrate:down
DROP TABLE "payments"."test_webhook_001"; 
