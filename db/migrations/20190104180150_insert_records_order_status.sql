-- migrate:up
INSERT INTO "payments"."order_status" VALUES (2, 'canceled');
INSERT INTO "payments"."order_status" VALUES (1, 'pending');
INSERT INTO "payments"."order_status" VALUES (3, 'completed');
INSERT INTO "payments"."order_status" VALUES (4, 'expired');

-- migrate:down
DELETE FROM "payments"."order_status";
