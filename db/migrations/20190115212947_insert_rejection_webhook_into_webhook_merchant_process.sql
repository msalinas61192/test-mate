-- migrate:up
INSERT INTO payments.webhook_merchant_process
(merchant_id, url, status_id)
VALUES(15, 'http://localhost:3000/webhooks/orders/rejection', 3);

-- migrate:down
DELETE FROM payments."webhook_merchant_process" WHERE merchant_id = 15 AND status_id = 3;
