-- migrate:up
ALTER TABLE payments."order" ADD expiration_date timestamp(6) without time zone NULL;
COMMENT ON COLUMN payments."order".expiration_date IS 'Fecha de expiración por defecto de la orden. Puede ser modificada segun medio de pago seleccionado.';

-- migrate:down
ALTER TABLE payments."order" DROP COLUMN expiration_date;
