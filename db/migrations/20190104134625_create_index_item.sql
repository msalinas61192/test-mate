-- migrate:up
CREATE UNIQUE INDEX "item_id_index" ON "payments"."item" USING btree (
  "id" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE INDEX "item_order_id_index" ON "payments"."item" USING btree (
  "order_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- migrate:down
DROP INDEX "payments"."item_id_index";
DROP INDEX "payments"."item_order_id_index";
