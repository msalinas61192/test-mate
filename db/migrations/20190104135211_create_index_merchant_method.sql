-- migrate:up
CREATE UNIQUE INDEX "merchant_method_id_index" ON "payments"."merchant_method" USING btree (
  "id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

CREATE UNIQUE INDEX "merchant_method_id_code" ON "payments"."merchant_method" USING btree (
  "method_code"
);

-- migrate:down
DROP INDEX "payments"."merchant_method_id_index";
DROP INDEX "payments"."merchant_method_id_code";

