-- migrate:up
CREATE TABLE "payments"."method" (
  "id"  SERIAL PRIMARY KEY,
  "code" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "image" text COLLATE "pg_catalog"."default",
  "description" varchar(300) COLLATE "pg_catalog"."default",
  "anonymous_payment" bool NOT NULL DEFAULT false,
  "max_anonymous_payment" int8 NOT NULL DEFAULT 50000,
  "enabled" bool NOT NULL DEFAULT true
)
;
COMMENT ON COLUMN "payments"."method"."id" IS 'Id del medio de pago';
COMMENT ON COLUMN "payments"."method"."code" IS 'Codigo ientificador del medio de pago';
COMMENT ON COLUMN "payments"."method"."name" IS 'Nombre del medio de pago.';
COMMENT ON COLUMN "payments"."method"."image" IS 'Ruta o base 64 de imagen del medio de pago.';
COMMENT ON COLUMN "payments"."method"."description" IS 'Descripción del medio de pago.';
COMMENT ON COLUMN "payments"."method"."anonymous_payment" IS 'Acepta pago anonimo';
COMMENT ON COLUMN "payments"."method"."max_anonymous_payment" IS 'Maximo monto de pago anonimo.';
COMMENT ON COLUMN "payments"."method"."enabled" IS 'Medio de pago habilitado.';


-- migrate:down
DROP TABLE IF EXISTS "payments"."method";
