-- migrate:up
CREATE TABLE "payments"."webhook_status" (
  "id" int4 NOT NULL,
  "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL
);

-- migrate:down
DROP TABLE IF EXISTS "payments"."webhook_status";

