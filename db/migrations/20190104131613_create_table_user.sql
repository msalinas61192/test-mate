-- migrate:up
CREATE TABLE "payments"."user" (
  "id" SERIAL PRIMARY KEY,
  "rut" varchar COLLATE "pg_catalog"."default",
  "email" varchar COLLATE "pg_catalog"."default",
  "order_id" int4
);

-- migrate:down
DROP TABLE IF EXISTS "payments"."user";
