-- migrate:up
ALTER TABLE "payments"."order" ADD CONSTRAINT "order_status_id_fkey" FOREIGN KEY ("status_id") REFERENCES "payments"."order_status" ("id") ON DELETE CASCADE ON UPDATE CASCADE;


-- migrate:down
ALTER TABLE "payments"."order" DROP CONSTRAINT "order_status_id_fkey";
