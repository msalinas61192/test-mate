-- migrate:up
CREATE TABLE "payments"."item" (
  "id" BIGSERIAL PRIMARY KEY,
  "code" varchar(15) COLLATE "pg_catalog"."default",
  "name" varchar(50) COLLATE "pg_catalog"."default",
  "price" int8,
  "unit_price" int8,
  "quantity" int4,
  "order_id" int8
)
;
COMMENT ON COLUMN "payments"."item"."id" IS 'Id item.';
COMMENT ON COLUMN "payments"."item"."code" IS 'Codigo item.';
COMMENT ON COLUMN "payments"."item"."name" IS 'Nombre.';
COMMENT ON COLUMN "payments"."item"."price" IS 'Precio.';
COMMENT ON COLUMN "payments"."item"."unit_price" IS 'Precio unitario.';
COMMENT ON COLUMN "payments"."item"."quantity" IS 'Cantidad.';
COMMENT ON COLUMN "payments"."item"."order_id" IS 'Id orden.';

-- migrate:down
DROP TABLE IF EXISTS "payments"."item";
