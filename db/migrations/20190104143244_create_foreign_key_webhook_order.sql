-- migrate:up
ALTER TABLE "payments"."webhook_order" ADD CONSTRAINT "fk_order_id" FOREIGN KEY ("order_id") REFERENCES "payments"."order" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "payments"."webhook_order" ADD CONSTRAINT "webhook_order_order_id_fkey" FOREIGN KEY ("order_id") REFERENCES "payments"."order" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;


-- migrate:down
ALTER TABLE "payments"."webhook_order" 
  DROP CONSTRAINT "fk_order_id",
  DROP CONSTRAINT "webhook_order_order_id_fkey";

