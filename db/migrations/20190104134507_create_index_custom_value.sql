-- migrate:up
CREATE UNIQUE INDEX "custom_value_id_index" ON "payments"."custom_value" USING btree (
  "id" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE INDEX "custom_value_order_id_index" ON "payments"."custom_value" USING btree (
  "order_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- migrate:down
DROP INDEX  "payments"."custom_value_id_index";
DROP INDEX  "payments"."custom_value_order_id_index";
