-- migrate:up
CREATE TABLE "payments"."merchant" (
  "id" SERIAL PRIMARY KEY,
  "rut" varchar(11) COLLATE "pg_catalog"."default",
  "business_name" varchar(150) COLLATE "pg_catalog"."default" NOT NULL,
  "brand_name" varchar(80) COLLATE "pg_catalog"."default" NOT NULL,
  "business_type" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "phone" int4 NOT NULL,
  "address" varchar(350) COLLATE "pg_catalog"."default" NOT NULL,
  "region_id" int4 NOT NULL,
  "comunne_id" int4 NOT NULL,
  "city_id" int4 NOT NULL,
  "manager_rut" int4 NOT NULL,
  "manager_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;
COMMENT ON COLUMN "payments"."merchant"."id" IS 'Id comercio.';
COMMENT ON COLUMN "payments"."merchant"."rut" IS 'Rut del comercio.';
COMMENT ON COLUMN "payments"."merchant"."business_name" IS 'Razon social del comercio.';
COMMENT ON COLUMN "payments"."merchant"."brand_name" IS 'Nombre de fantasia del comercio.';
COMMENT ON COLUMN "payments"."merchant"."business_type" IS 'Rubro en que se desempeña comercio.';
COMMENT ON COLUMN "payments"."merchant"."phone" IS 'Numero de telefono.';
COMMENT ON COLUMN "payments"."merchant"."address" IS 'Direccion';
COMMENT ON COLUMN "payments"."merchant"."region_id" IS 'Region.';
COMMENT ON COLUMN "payments"."merchant"."comunne_id" IS 'Comuna.';
COMMENT ON COLUMN "payments"."merchant"."city_id" IS 'Ciudad.';
COMMENT ON COLUMN "payments"."merchant"."manager_rut" IS 'RUT representante.';
COMMENT ON COLUMN "payments"."merchant"."manager_name" IS 'Nombre del representante.';


-- migrate:down
DROP TABLE IF EXISTS "payments"."merchant";
