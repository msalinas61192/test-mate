-- migrate:up
CREATE INDEX "consumer_kong_kong_id_idx" ON "payments"."consumer_kong" USING btree (
  "kong_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "consumer_kong_merchant_id_idx" ON "payments"."consumer_kong" USING btree (
  "merchant_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "customer_kong_id" ON "payments"."consumer_kong" USING btree (
  "id" "pg_catalog"."int8_ops" ASC NULLS LAST
);


-- migrate:down
DROP INDEX "payments"."consumer_kong_kong_id_idx";
DROP INDEX "payments"."consumer_kong_merchant_id_idx";
DROP INDEX "payments"."customer_kong_id";
