-- migrate:up
CREATE TABLE "payments"."event_order" (
  "id" BIGSERIAL PRIMARY KEY,
  "code" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "body" json NOT NULL,
  "id_order" int8 NOT NULL
)
;
COMMENT ON TABLE "payments"."event_order" IS 'registo de eventos de pasarela';

-- migrate:down
DROP TABLE IF EXISTS "payments"."event_order";

