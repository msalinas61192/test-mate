-- migrate:up
INSERT INTO payments."order" (reference_id,description,redirect_url,status_id,url_return,url_cancel,method_id,method_list,merchant_id,payer,total,currency,total_details,creation_date,modification_date,expiration_date) VALUES 
('3','mi orden','http://mi.url',1,'http://return.cl','http://urlcandel.cl',5,'["paypal", "tarjetas", "efectivo"]',15,'{"email" : "usr.dom.cl", "nombre" : "Andrés Soto"}',130000,'CLP','{"subtotal" : "39000", "fee" : "1000", "tax" : "1600"}',NULL,NULL,'2018-09-11 05:20:14.000')
;

-- migrate:down
DELETE FROM payments."order" WHERE reference_id = '3' AND description = 'mi orden' AND status_id = 1;
