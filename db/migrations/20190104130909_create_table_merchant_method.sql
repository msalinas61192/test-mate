-- migrate:up
CREATE TABLE "payments"."merchant_method" (
  "id" SERIAL PRIMARY KEY,
  "method_code" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "merchant_code" int4
)
;
COMMENT ON COLUMN "payments"."merchant_method"."id" IS 'Id comercio - lista de pagos del comercio.';
COMMENT ON COLUMN "payments"."merchant_method"."method_code" IS 'Codigo del metodo de pago disponible para ecommerce.';

-- migrate:down
DROP TABLE IF EXISTS "payments"."merchant_method";
