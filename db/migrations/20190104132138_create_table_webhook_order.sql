-- migrate:up
CREATE TABLE "payments"."webhook_order" (
  "id" BIGSERIAL PRIMARY KEY,
  "order_id" int8,
  "url" varchar(300) COLLATE "pg_catalog"."default",
  "status_id" int4,
  "merchant_id" int8
);

-- migrate:down
DROP TABLE IF EXISTS "payments"."webhook_order";
