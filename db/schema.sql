SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: payments; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA payments;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: consumer_kong; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments.consumer_kong (
    id bigint NOT NULL,
    merchant_id bigint,
    kong_id character varying(50),
    algorithm_kong character varying(50),
    key_kong character varying(50),
    secret_kong character varying(50),
    consumer_kong character varying(50),
    api_key character varying(50),
    iss character varying(50),
    apikey character varying(255)
);


--
-- Name: COLUMN consumer_kong.id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.consumer_kong.id IS 'Id del kong merchant (consumer)';


--
-- Name: COLUMN consumer_kong.merchant_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.consumer_kong.merchant_id IS 'Id del merchant.';


--
-- Name: COLUMN consumer_kong.kong_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.consumer_kong.kong_id IS 'Id creacion merchant (consumer) Kong.';


--
-- Name: COLUMN consumer_kong.algorithm_kong; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.consumer_kong.algorithm_kong IS 'Id algoritmo codificación merchant (consumer) Kong.';


--
-- Name: COLUMN consumer_kong.key_kong; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.consumer_kong.key_kong IS 'Id para generación token Kong.';


--
-- Name: COLUMN consumer_kong.secret_kong; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.consumer_kong.secret_kong IS 'Llave de encriptación token Kong.';


--
-- Name: COLUMN consumer_kong.consumer_kong; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.consumer_kong.consumer_kong IS 'Id merchant (consumer) en kong.';


--
-- Name: COLUMN consumer_kong.api_key; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.consumer_kong.api_key IS 'Id (permamente) para consumir APIs.';


--
-- Name: COLUMN consumer_kong.iss; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.consumer_kong.iss IS 'Identificador para consumir API.';


--
-- Name: consumer_kong_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.consumer_kong_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: consumer_kong_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.consumer_kong_id_seq OWNED BY payments.consumer_kong.id;


--
-- Name: custom_value; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments.custom_value (
    id bigint NOT NULL,
    key character varying(30),
    value character varying(300),
    order_id bigint NOT NULL
);


--
-- Name: COLUMN custom_value.id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.custom_value.id IS 'Id custom value.';


--
-- Name: COLUMN custom_value.key; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.custom_value.key IS 'Llave del registro.';


--
-- Name: COLUMN custom_value.value; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.custom_value.value IS 'Valor del registro.';


--
-- Name: COLUMN custom_value.order_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.custom_value.order_id IS 'Id de orden.';


--
-- Name: custom_value_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.custom_value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: custom_value_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.custom_value_id_seq OWNED BY payments.custom_value.id;


--
-- Name: event_order; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments.event_order (
    id bigint NOT NULL,
    code character varying(15) NOT NULL,
    body json NOT NULL,
    id_order bigint NOT NULL
);


--
-- Name: TABLE event_order; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON TABLE payments.event_order IS 'registo de eventos de pasarela';


--
-- Name: event_order_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.event_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: event_order_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.event_order_id_seq OWNED BY payments.event_order.id;


--
-- Name: item; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments.item (
    id bigint NOT NULL,
    code character varying(15),
    name character varying(50),
    price bigint,
    unit_price bigint,
    quantity integer,
    order_id bigint
);


--
-- Name: COLUMN item.id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.item.id IS 'Id item.';


--
-- Name: COLUMN item.code; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.item.code IS 'Codigo item.';


--
-- Name: COLUMN item.name; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.item.name IS 'Nombre.';


--
-- Name: COLUMN item.price; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.item.price IS 'Precio.';


--
-- Name: COLUMN item.unit_price; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.item.unit_price IS 'Precio unitario.';


--
-- Name: COLUMN item.quantity; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.item.quantity IS 'Cantidad.';


--
-- Name: COLUMN item.order_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.item.order_id IS 'Id orden.';


--
-- Name: item_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: item_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.item_id_seq OWNED BY payments.item.id;


--
-- Name: merchant; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments.merchant (
    id integer NOT NULL,
    rut character varying(11),
    business_name character varying(150) NOT NULL,
    brand_name character varying(80) NOT NULL,
    business_type character varying(255) NOT NULL,
    phone integer NOT NULL,
    address character varying(350) NOT NULL,
    region_id integer NOT NULL,
    comunne_id integer NOT NULL,
    city_id integer NOT NULL,
    manager_rut integer NOT NULL,
    manager_name character varying(255) NOT NULL
);


--
-- Name: COLUMN merchant.id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.id IS 'Id comercio.';


--
-- Name: COLUMN merchant.rut; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.rut IS 'Rut del comercio.';


--
-- Name: COLUMN merchant.business_name; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.business_name IS 'Razon social del comercio.';


--
-- Name: COLUMN merchant.brand_name; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.brand_name IS 'Nombre de fantasia del comercio.';


--
-- Name: COLUMN merchant.business_type; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.business_type IS 'Rubro en que se desempeña comercio.';


--
-- Name: COLUMN merchant.phone; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.phone IS 'Numero de telefono.';


--
-- Name: COLUMN merchant.address; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.address IS 'Direccion';


--
-- Name: COLUMN merchant.region_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.region_id IS 'Region.';


--
-- Name: COLUMN merchant.comunne_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.comunne_id IS 'Comuna.';


--
-- Name: COLUMN merchant.city_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.city_id IS 'Ciudad.';


--
-- Name: COLUMN merchant.manager_rut; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.manager_rut IS 'RUT representante.';


--
-- Name: COLUMN merchant.manager_name; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant.manager_name IS 'Nombre del representante.';


--
-- Name: merchant_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.merchant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: merchant_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.merchant_id_seq OWNED BY payments.merchant.id;


--
-- Name: merchant_method; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments.merchant_method (
    id integer NOT NULL,
    method_code character varying(15) NOT NULL,
    merchant_code integer
);


--
-- Name: COLUMN merchant_method.id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant_method.id IS 'Id comercio - lista de pagos del comercio.';


--
-- Name: COLUMN merchant_method.method_code; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.merchant_method.method_code IS 'Codigo del metodo de pago disponible para ecommerce.';


--
-- Name: merchant_method_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.merchant_method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: merchant_method_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.merchant_method_id_seq OWNED BY payments.merchant_method.id;


--
-- Name: method; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments.method (
    id integer NOT NULL,
    code character varying(15) NOT NULL,
    name character varying(50) NOT NULL,
    image text,
    description character varying(300),
    anonymous_payment boolean DEFAULT false NOT NULL,
    max_anonymous_payment bigint DEFAULT 50000 NOT NULL,
    enabled boolean DEFAULT true NOT NULL
);


--
-- Name: COLUMN method.id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.method.id IS 'Id del medio de pago';


--
-- Name: COLUMN method.code; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.method.code IS 'Codigo ientificador del medio de pago';


--
-- Name: COLUMN method.name; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.method.name IS 'Nombre del medio de pago.';


--
-- Name: COLUMN method.image; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.method.image IS 'Ruta o base 64 de imagen del medio de pago.';


--
-- Name: COLUMN method.description; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.method.description IS 'Descripción del medio de pago.';


--
-- Name: COLUMN method.anonymous_payment; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.method.anonymous_payment IS 'Acepta pago anonimo';


--
-- Name: COLUMN method.max_anonymous_payment; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.method.max_anonymous_payment IS 'Maximo monto de pago anonimo.';


--
-- Name: COLUMN method.enabled; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.method.enabled IS 'Medio de pago habilitado.';


--
-- Name: method_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: method_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.method_id_seq OWNED BY payments.method.id;


--
-- Name: order; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments."order" (
    id bigint NOT NULL,
    reference_id character varying(30),
    description character varying(300),
    redirect_url character varying(300),
    status_id integer,
    url_return character varying(300),
    url_cancel character varying(300),
    method_id integer,
    method_list json,
    merchant_id bigint NOT NULL,
    payer json,
    total bigint DEFAULT 0,
    currency character varying(3),
    total_details json,
    creation_date timestamp(6) without time zone,
    modification_date timestamp(6) without time zone
);


--
-- Name: COLUMN "order".id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".id IS 'Id orden.';


--
-- Name: COLUMN "order".reference_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".reference_id IS 'Identificacion del merchant (comercio).';


--
-- Name: COLUMN "order".description; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".description IS 'Descripcion de la orden.';


--
-- Name: COLUMN "order".redirect_url; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".redirect_url IS 'URL redireccion pasarela Multicaja.';


--
-- Name: COLUMN "order".status_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".status_id IS 'Estado de orden.';


--
-- Name: COLUMN "order".url_return; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".url_return IS 'URL redireccion merchant.';


--
-- Name: COLUMN "order".url_cancel; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".url_cancel IS 'URL cancelación merchant.';


--
-- Name: COLUMN "order".method_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".method_id IS 'Id tipo de pago seleccionado.';


--
-- Name: COLUMN "order".method_list; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".method_list IS 'Listado tipos de pago merchant.';


--
-- Name: COLUMN "order".merchant_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".merchant_id IS 'Id comercio.';


--
-- Name: COLUMN "order".payer; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".payer IS 'Identificador del cliente que efectua pago de una orden.';


--
-- Name: COLUMN "order".creation_date; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".creation_date IS 'Fecha de creacion orden.';


--
-- Name: COLUMN "order".modification_date; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments."order".modification_date IS 'Fecha de modificacion orden.';


--
-- Name: order_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.order_id_seq OWNED BY payments."order".id;


--
-- Name: order_status; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments.order_status (
    id integer NOT NULL,
    name character varying(50)
);


--
-- Name: COLUMN order_status.id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.order_status.id IS 'Id estado de orden';


--
-- Name: COLUMN order_status.name; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.order_status.name IS 'Nombre estado de orden.';


--
-- Name: order_status_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.order_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: order_status_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.order_status_id_seq OWNED BY payments.order_status.id;


--
-- Name: user; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments."user" (
    id integer NOT NULL,
    rut character varying,
    email character varying,
    order_id integer
);


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.user_id_seq OWNED BY payments."user".id;


--
-- Name: webhook_merchant_process; Type: TABLE; Schema: payments; Owner: -
--

CREATE TABLE payments.webhook_merchant_process (
    id integer NOT NULL,
    merchant_id bigint,
    url character varying(300),
    status_id integer
);


--
-- Name: TABLE webhook_merchant_process; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON TABLE payments.webhook_merchant_process IS 'Lista de webhook a ejecutar por estado de proceso y merchant.';


--
-- Name: COLUMN webhook_merchant_process.status_id; Type: COMMENT; Schema: payments; Owner: -
--

COMMENT ON COLUMN payments.webhook_merchant_process.status_id IS 'Estado de webhook';


--
-- Name: webhook_merchant_process_id_seq; Type: SEQUENCE; Schema: payments; Owner: -
--

CREATE SEQUENCE payments.webhook_merchant_process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: webhook_merchant_process_id_seq; Type: SEQUENCE OWNED BY; Schema: payments; Owner: -
--

ALTER SEQUENCE payments.webhook_merchant_process_id_seq OWNED BY payments.webhook_merchant_process.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: consumer_kong id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.consumer_kong ALTER COLUMN id SET DEFAULT nextval('payments.consumer_kong_id_seq'::regclass);


--
-- Name: custom_value id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.custom_value ALTER COLUMN id SET DEFAULT nextval('payments.custom_value_id_seq'::regclass);


--
-- Name: event_order id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.event_order ALTER COLUMN id SET DEFAULT nextval('payments.event_order_id_seq'::regclass);


--
-- Name: item id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.item ALTER COLUMN id SET DEFAULT nextval('payments.item_id_seq'::regclass);


--
-- Name: merchant id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.merchant ALTER COLUMN id SET DEFAULT nextval('payments.merchant_id_seq'::regclass);


--
-- Name: merchant_method id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.merchant_method ALTER COLUMN id SET DEFAULT nextval('payments.merchant_method_id_seq'::regclass);


--
-- Name: method id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.method ALTER COLUMN id SET DEFAULT nextval('payments.method_id_seq'::regclass);


--
-- Name: order id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments."order" ALTER COLUMN id SET DEFAULT nextval('payments.order_id_seq'::regclass);


--
-- Name: order_status id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.order_status ALTER COLUMN id SET DEFAULT nextval('payments.order_status_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments."user" ALTER COLUMN id SET DEFAULT nextval('payments.user_id_seq'::regclass);


--
-- Name: webhook_merchant_process id; Type: DEFAULT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.webhook_merchant_process ALTER COLUMN id SET DEFAULT nextval('payments.webhook_merchant_process_id_seq'::regclass);


--
-- Name: consumer_kong consumer_kong_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.consumer_kong
    ADD CONSTRAINT consumer_kong_pkey PRIMARY KEY (id);


--
-- Name: custom_value custom_value_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.custom_value
    ADD CONSTRAINT custom_value_pkey PRIMARY KEY (id);


--
-- Name: event_order event_order_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.event_order
    ADD CONSTRAINT event_order_pkey PRIMARY KEY (id);


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (id);


--
-- Name: merchant_method merchant_method_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.merchant_method
    ADD CONSTRAINT merchant_method_pkey PRIMARY KEY (id);


--
-- Name: merchant merchant_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.merchant
    ADD CONSTRAINT merchant_pkey PRIMARY KEY (id);


--
-- Name: method method_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.method
    ADD CONSTRAINT method_pkey PRIMARY KEY (id);


--
-- Name: order order_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments."order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);


--
-- Name: order_status order_status_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.order_status
    ADD CONSTRAINT order_status_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: webhook_merchant_process webhook_merchant_process_pkey; Type: CONSTRAINT; Schema: payments; Owner: -
--

ALTER TABLE ONLY payments.webhook_merchant_process
    ADD CONSTRAINT webhook_merchant_process_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- PostgreSQL database dump complete
--


--
-- Dbmate schema migrations
--

INSERT INTO public.schema_migrations (version) VALUES
    ('20190103212315'),
    ('20190103212325'),
    ('20190104125739'),
    ('20190104130358'),
    ('20190104130505'),
    ('20190104130617'),
    ('20190104130909'),
    ('20190104131001'),
    ('20190104131152'),
    ('20190104131248'),
    ('20190104131613'),
    ('20190104131732');
