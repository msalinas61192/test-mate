#GENERATE ALPINE IMAGE
FROM debian:stable-slim

#MANTAINER INFO
LABEL maintainer="matias.salinas@multicaja.cl"

#SET ENVIRIOMENT VARIABLES
ENV USER_DB=payments@prepaid-postgresql-staging
ENV PASS_DB=payments
ENV IP_DB=prepaid-postgresql-staging.postgres.database.azure.com
ENV PORT_DB=5432
ENV NAME_DB=test_migration
ENV SSL_DB=disable


RUN mkdir /app
COPY . /app
WORKDIR /app

#SET DB_CONN
ENV CONN="postgres://${USER_DB}:${PASS_DB}@${IP_DB}:${PORT_DB}/${NAME_DB}?sslmode=${SSL_DB}"
RUN echo "DATABASE_URL=\"${CONN}\"" > .env

RUN cat .env

RUN ls -la .
RUN pwd
RUN ls -la ./dbmate


#SET ENTRYPOINT
ENTRYPOINT ["sh", "-c","./dbmate up"]
