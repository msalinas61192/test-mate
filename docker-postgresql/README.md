# Postgresql

## Comandos para el contendor

### Configurar contenedor

    ./config

### Crear contenedor

    ./build

### Eliminar contenedor existente

    ./kill

### Iniciar contenedor existente

    ./start

### Detener contenedor existente

    ./stop

### Ver logs del contendor existente

    ./logs

## Pasos para iniciar crear el contendor por primera vez

    ./config
    ./build
