# migrations-payments-gateway

## 1. Iniciar docker postgresql

Información [Aquí](./docker-postgresql/README.md)

## 2. Instalar `dbmate`

### En `osx`

    brew tap amacneil/dbmate
    brew install dbmate

### En `linux`

    sudo curl -fsSL -o /usr/local/bin/dbmate https://github.com/amacneil/dbmate/releases/download/v1.4.1/dbmate-linux-amd64
    sudo chmod +x /usr/local/bin/dbmate

## 3. Ejecutar migraciones

    ./dbmate up

## Comandos dbmate

    dbmate           # print help
    dbmate new       # generate a new migration file
    dbmate up        # create the database (if it does not already exist) and run any pending migrations
    dbmate create    # create the database
    dbmate drop      # drop the database
    dbmate migrate   # run any pending migrations
    dbmate rollback  # roll back the most recent migration
    dbmate down      # alias for rollback
    dbmate dump      # write the database schema.sql file
    dbmate wait      # wait for the database server to become available

